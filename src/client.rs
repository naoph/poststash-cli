use serde::{de::DeserializeOwned, Serialize};
use snafu::prelude::*;
use url::Url;

use poststash::api::request::*;
use poststash::api::response::*;

pub struct Client {
    reqwest: reqwest::Client,
    server: Url,
}

impl Client {
    pub fn init(user_agent: String, server: Url) -> Client {
        let rc = reqwest::ClientBuilder::new()
            .user_agent(user_agent)
            .build()
            .expect("Failed to build http client");

        Client { reqwest: rc, server }
    }

    async fn gen_url(&self, path: &str) -> String {
        let deslashed = if path.starts_with("/") {
            let mut chars = path.chars();
            chars.next();
            chars.as_str()
        } else {
            path
        };
        format!("{}{}", self.server, deslashed)
    }

    pub async fn get_json<O: DeserializeOwned>(&self, path: &str) -> Result<O, RequestError> {
        let url = self.gen_url(path).await;
        let response = self.reqwest.get(url)
            .send()
            .await
            .context(ErrorMakingRequestSnafu)?;
        let out = response.json()
            .await
            .context(ErrorDeserializingRequestSnafu)?;
        Ok(out)
    }

    pub async fn post_json<I: Serialize, O: DeserializeOwned>(&self, path: &str, body: I) -> Result<O, RequestError> {
        let url = self.gen_url(path).await;
        let response = self.reqwest.post(url)
            .json(&body)
            .send()
            .await
            .context(ErrorMakingRequestSnafu)?;
        let out = response.json()
            .await
            .context(ErrorDeserializingRequestSnafu)?;
        Ok(out)
    }

    pub async fn document_create(&self, req: DocumentCreateReq) -> Result<DocumentCreateResp, RequestError> {
        let path = "/document/create";
        self.post_json(path, req).await
    }

    pub async fn document_create_status(&self, url: &Url) -> Result<DocumentCreateStatusRespWrap, RequestError> {
        let enc_url = urlencoding::encode(url.as_str());
        let path = format!("/document/create/status/{enc_url}");
        self.get_json(&path).await
    }
}

#[derive(Debug, Snafu)]
pub enum RequestError {
    #[snafu(display("Error making request: {source}"))]
    ErrorMakingRequest { source: reqwest::Error },

    #[snafu(display("Error deserializing request: {source}"))]
    ErrorDeserializingRequest { source: reqwest::Error },
}
