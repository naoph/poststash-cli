use clap::{Args, Parser, Subcommand};
use url::Url;

#[derive(Debug, Parser)]
#[clap(author, version, about)]
pub struct Cli {
    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Debug, Subcommand)]
pub enum Commands {
    /// Document operations
    Document(Document),

    /// Tag operations
    Tag(Tag),
}

#[derive(Debug, Args)]
pub struct Document {
    #[clap(subcommand)]
    pub command: DocumentCommands,
}

#[derive(Debug, Subcommand)]
pub enum DocumentCommands {
    /// Extract a new document from a given URL
    Extract { url: Url },

    /// List the most recently added documents
    Recent,
}

#[derive(Debug, Args)]
pub struct Tag {
    #[clap(subcommand)]
    pub command: TagCommands,
}

#[derive(Debug, Subcommand)]
pub enum TagCommands {
    /// Edit a tag's description and implications
    Edit,

    /// List tags
    List,
}
