#[macro_use] extern crate lazy_static;

mod ap;
mod client;
mod config;
mod exec;

use clap::Parser;

lazy_static! {
    pub static ref CONFIG_PATH: String = {
        match std::env::var("POSTSTASH_CLI_CONF") {
            Ok(p) => p,
            Err(_) => {
                let path = dirs::home_dir()
                    .expect("Couldn't find config. Try setting $POSTSTASH_CLI_CONF or $HOME");
                let path = path.join(".config")
                    .join("poststash")
                    .join("cli.conf");
                match path.to_str() {
                    None => panic!("Config path {:?} is not legal", path),
                    Some(s) => s.to_string(),
                }
            },
        }
    };
    pub static ref CONFIG: config::Config = {
        match config::Config::load(&*CONFIG_PATH) {
            Ok(c) => c,
            Err(e) => panic!("Error loading config `{}`: {}", &*CONFIG_PATH, e),
        }
    };
    pub static ref CLIENT: client::Client = {
        let ua = concat!(
            env!("CARGO_PKG_NAME"),
            "/",
            env!("CARGO_PKG_VERSION"),
        );
        client::Client::init(ua.to_string(), CONFIG.server.clone())
    };
}

#[tokio::main]
async fn main() {
    let cli = ap::Cli::parse();

    exec::exec(cli).await;
}
