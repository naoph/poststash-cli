use crate::ap::*;

mod document;
mod tag;

pub async fn exec(cli: Cli) {
    match cli.command {
        Commands::Document(d) => document::exec(d).await,
        Commands::Tag(t) => tag::exec(t).await,
    }
}
