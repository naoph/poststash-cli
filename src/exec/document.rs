use std::io::Write;

use url::Url;

use poststash::api::request::*;
use poststash::api::response::*;

use crate::CLIENT;
use crate::ap::*;

pub async fn exec(d: Document) {
    match d.command {
        DocumentCommands::Extract { url } => extract(url).await,
        DocumentCommands::Recent => recent().await,
    }
}

async fn extract(url: Url) {
    // Make initial request to initiate
    let initial_request = DocumentCreateReq { url: url.clone() };
    let initial_response = CLIENT.document_create(initial_request).await;
    let initial_response = match initial_response {
        Err(e) => {
            eprintln!("Unexpected error: {e}");
            return;
        },
        Ok(r) => r,
    };
    match initial_response {
        DocumentCreateResp::InvalidUrl => {
            eprintln!("URL is invalid");
            return;
        },
        DocumentCreateResp::UnhandledUrl => {
            eprintln!("URL is not yet supported");
            return;
        },
        DocumentCreateResp::Duplicate => {
            eprintln!("This URL is already being extracted or has already been extracted");
            return;
        },
        DocumentCreateResp::Started { extractor } => {
            print!("Began extraction with {extractor} ");
            let _ = std::io::stdout().flush();
        },
    }

    // Check for updates
    let mut delay_ms = 600;
    for _ in 0..20 {
        tokio::time::sleep(tokio::time::Duration::from_millis(delay_ms)).await;
        delay_ms = (delay_ms as f64 * 1.2) as u64;
        let update_response = CLIENT.document_create_status(&url).await;
        let update_response = match update_response {
            Err(e) => {
                eprintln!("\nUnexpected error: {e}");
                return;
            },
            Ok(r) => r,
        };
        match update_response {
            DocumentCreateStatusRespWrap::InvalidUrl => {
                eprintln!("\nURL is invalid");
                return;
            },
            DocumentCreateStatusRespWrap::InvalidEncodedUrl => {
                eprintln!("\nEncoded URL is invalid");
                return;
            },
            DocumentCreateStatusRespWrap::UnknownUrl => {
                eprintln!("\nURL has not been seen before");
                return;
            },
            DocumentCreateStatusRespWrap::Exists { status } => {
                match status {
                    DocumentCreateStatusResp::Ongoing => {
                        print!(".");
                        let _ = std::io::stdout().flush();
                    },
                    DocumentCreateStatusResp::Failure { message } => {
                        eprintln!("\nExtraction failed: {message}");
                        return;
                    },
                    DocumentCreateStatusResp::Success { document_id } => {
                        println!("\nExtraction succeeded, new document has ID {document_id}");
                        return;
                    },
                }
            }
        }
    }

    // Give up on waiting after a while
    println!("\nExtraction has been ongoing for a while, continuing in background")
}

async fn recent() {
    println!("Listing recent documents");
}
