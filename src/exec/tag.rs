use crate::ap::*;

pub async fn exec(t: Tag) {
    match t.command {
        TagCommands::Edit => edit().await,
        TagCommands::List => list().await,
    }
}

async fn edit() {
    println!("Editing tag");
}

async fn list() {
    println!("Listing tags");
}
